(function () {
    document.querySelector('.date_year').textContent = new Date().getFullYear();

    const lat = document.querySelector('span.lat');
    const long = document.querySelector('span.long');
    const time = document.querySelector('span.time');

    const mymap = L.map('issMap').setView([0, 0], 5);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(
        mymap,
    );

    const myIcon = L.icon({
        iconUrl: 'iss.png',
        iconSize: [50, 32],
        iconAnchor: [30, 10],
    });

    const marker = L.marker([0, 0], { icon: myIcon }).addTo(mymap);

    const API_URL = 'https://api.wheretheiss.at/v1/satellites/25544';
    let firstTime = true;

    function getValueFixed(value) {
        return value.toFixed(2) + '°';
    }

    async function getDateISS() {
        const res = await fetch(API_URL);
        const date = await res.json();
        const { latitude, longitude } = date;
        lat.textContent = getValueFixed(latitude);
        long.textContent = getValueFixed(longitude);
        time.textContent = new Date().toLocaleTimeString();

        if (firstTime) {
            marker
                .bindPopup(`If you're looking for me, here I am!`)
                .openPopup();
            firstTime = false;
        }
        marker.addEventListener('click', function (e) {
            mymap.panTo(this.getLatLng());
        });
        marker.setLatLng([latitude, longitude]);

        mymap.setView([latitude, longitude]);
    }

    let pause = false;
    let button = document.querySelector('button');
    let intISS = setInterval(getDateISS, 1000);

    button.addEventListener('click', () => {
        pause = !pause;

        if (pause) {
            button.textContent = 'Start';
            clearInterval(intISS);
        } else {
            button.textContent = 'Pause';
            intISS = setInterval(getDateISS, 1000);
        }
    });
    getDateISS();
})();
